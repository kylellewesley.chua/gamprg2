// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ENDLESSRUNNERCPP_EndlessRunnerCppGameMode_generated_h
#error "EndlessRunnerCppGameMode.generated.h already included, missing '#pragma once' in EndlessRunnerCppGameMode.h"
#endif
#define ENDLESSRUNNERCPP_EndlessRunnerCppGameMode_generated_h

#define EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppGameMode_h_12_SPARSE_DATA
#define EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppGameMode_h_12_RPC_WRAPPERS
#define EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEndlessRunnerCppGameMode(); \
	friend struct Z_Construct_UClass_AEndlessRunnerCppGameMode_Statics; \
public: \
	DECLARE_CLASS(AEndlessRunnerCppGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunnerCpp"), ENDLESSRUNNERCPP_API) \
	DECLARE_SERIALIZER(AEndlessRunnerCppGameMode)


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAEndlessRunnerCppGameMode(); \
	friend struct Z_Construct_UClass_AEndlessRunnerCppGameMode_Statics; \
public: \
	DECLARE_CLASS(AEndlessRunnerCppGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunnerCpp"), ENDLESSRUNNERCPP_API) \
	DECLARE_SERIALIZER(AEndlessRunnerCppGameMode)


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ENDLESSRUNNERCPP_API AEndlessRunnerCppGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEndlessRunnerCppGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENDLESSRUNNERCPP_API, AEndlessRunnerCppGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEndlessRunnerCppGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENDLESSRUNNERCPP_API AEndlessRunnerCppGameMode(AEndlessRunnerCppGameMode&&); \
	ENDLESSRUNNERCPP_API AEndlessRunnerCppGameMode(const AEndlessRunnerCppGameMode&); \
public:


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENDLESSRUNNERCPP_API AEndlessRunnerCppGameMode(AEndlessRunnerCppGameMode&&); \
	ENDLESSRUNNERCPP_API AEndlessRunnerCppGameMode(const AEndlessRunnerCppGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENDLESSRUNNERCPP_API, AEndlessRunnerCppGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEndlessRunnerCppGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AEndlessRunnerCppGameMode)


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppGameMode_h_9_PROLOG
#define EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppGameMode_h_12_SPARSE_DATA \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppGameMode_h_12_RPC_WRAPPERS \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppGameMode_h_12_INCLASS \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppGameMode_h_12_SPARSE_DATA \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppGameMode_h_12_INCLASS_NO_PURE_DECLS \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENDLESSRUNNERCPP_API UClass* StaticClass<class AEndlessRunnerCppGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
