// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ATile;
class UPrimitiveComponent;
class AActor;
#ifdef ENDLESSRUNNERCPP_Tile_generated_h
#error "Tile.generated.h already included, missing '#pragma once' in Tile.h"
#endif
#define ENDLESSRUNNERCPP_Tile_generated_h

#define EndlessRunnerCpp_Source_EndlessRunnerCpp_Tile_h_12_DELEGATE \
struct _Script_EndlessRunnerCpp_eventExitTriggerSignature_Parms \
{ \
	ATile* Tile; \
}; \
static inline void FExitTriggerSignature_DelegateWrapper(const FMulticastScriptDelegate& ExitTriggerSignature, ATile* Tile) \
{ \
	_Script_EndlessRunnerCpp_eventExitTriggerSignature_Parms Parms; \
	Parms.Tile=Tile; \
	ExitTriggerSignature.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_Tile_h_17_SPARSE_DATA
#define EndlessRunnerCpp_Source_EndlessRunnerCpp_Tile_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSpawnPickupCoin); \
	DECLARE_FUNCTION(execSpawnObstacles); \
	DECLARE_FUNCTION(execDestroyTile); \
	DECLARE_FUNCTION(execOnExit);


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_Tile_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSpawnPickupCoin); \
	DECLARE_FUNCTION(execSpawnObstacles); \
	DECLARE_FUNCTION(execDestroyTile); \
	DECLARE_FUNCTION(execOnExit);


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_Tile_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATile(); \
	friend struct Z_Construct_UClass_ATile_Statics; \
public: \
	DECLARE_CLASS(ATile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunnerCpp"), NO_API) \
	DECLARE_SERIALIZER(ATile)


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_Tile_h_17_INCLASS \
private: \
	static void StaticRegisterNativesATile(); \
	friend struct Z_Construct_UClass_ATile_Statics; \
public: \
	DECLARE_CLASS(ATile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunnerCpp"), NO_API) \
	DECLARE_SERIALIZER(ATile)


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_Tile_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATile(ATile&&); \
	NO_API ATile(const ATile&); \
public:


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_Tile_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATile(ATile&&); \
	NO_API ATile(const ATile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATile)


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_Tile_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SceneComponent() { return STRUCT_OFFSET(ATile, SceneComponent); } \
	FORCEINLINE static uint32 __PPO__AttachPoint() { return STRUCT_OFFSET(ATile, AttachPoint); } \
	FORCEINLINE static uint32 __PPO__ExitTrigger() { return STRUCT_OFFSET(ATile, ExitTrigger); } \
	FORCEINLINE static uint32 __PPO__BoundingBox() { return STRUCT_OFFSET(ATile, BoundingBox); } \
	FORCEINLINE static uint32 __PPO__CoinBoundingBox() { return STRUCT_OFFSET(ATile, CoinBoundingBox); }


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_Tile_h_14_PROLOG
#define EndlessRunnerCpp_Source_EndlessRunnerCpp_Tile_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_Tile_h_17_PRIVATE_PROPERTY_OFFSET \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_Tile_h_17_SPARSE_DATA \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_Tile_h_17_RPC_WRAPPERS \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_Tile_h_17_INCLASS \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_Tile_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_Tile_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_Tile_h_17_PRIVATE_PROPERTY_OFFSET \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_Tile_h_17_SPARSE_DATA \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_Tile_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_Tile_h_17_INCLASS_NO_PURE_DECLS \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_Tile_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENDLESSRUNNERCPP_API UClass* StaticClass<class ATile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID EndlessRunnerCpp_Source_EndlessRunnerCpp_Tile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
