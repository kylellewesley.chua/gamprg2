// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ENDLESSRUNNERCPP_EndlessRunnerCppCharacter_generated_h
#error "EndlessRunnerCppCharacter.generated.h already included, missing '#pragma once' in EndlessRunnerCppCharacter.h"
#endif
#define ENDLESSRUNNERCPP_EndlessRunnerCppCharacter_generated_h

#define EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppCharacter_h_12_SPARSE_DATA
#define EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppCharacter_h_12_RPC_WRAPPERS
#define EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppCharacter_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEndlessRunnerCppCharacter(); \
	friend struct Z_Construct_UClass_AEndlessRunnerCppCharacter_Statics; \
public: \
	DECLARE_CLASS(AEndlessRunnerCppCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunnerCpp"), NO_API) \
	DECLARE_SERIALIZER(AEndlessRunnerCppCharacter)


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppCharacter_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAEndlessRunnerCppCharacter(); \
	friend struct Z_Construct_UClass_AEndlessRunnerCppCharacter_Statics; \
public: \
	DECLARE_CLASS(AEndlessRunnerCppCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunnerCpp"), NO_API) \
	DECLARE_SERIALIZER(AEndlessRunnerCppCharacter)


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppCharacter_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEndlessRunnerCppCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEndlessRunnerCppCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEndlessRunnerCppCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEndlessRunnerCppCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEndlessRunnerCppCharacter(AEndlessRunnerCppCharacter&&); \
	NO_API AEndlessRunnerCppCharacter(const AEndlessRunnerCppCharacter&); \
public:


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEndlessRunnerCppCharacter(AEndlessRunnerCppCharacter&&); \
	NO_API AEndlessRunnerCppCharacter(const AEndlessRunnerCppCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEndlessRunnerCppCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEndlessRunnerCppCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AEndlessRunnerCppCharacter)


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AEndlessRunnerCppCharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(AEndlessRunnerCppCharacter, FollowCamera); }


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppCharacter_h_9_PROLOG
#define EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppCharacter_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppCharacter_h_12_SPARSE_DATA \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppCharacter_h_12_RPC_WRAPPERS \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppCharacter_h_12_INCLASS \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppCharacter_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppCharacter_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppCharacter_h_12_SPARSE_DATA \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppCharacter_h_12_INCLASS_NO_PURE_DECLS \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENDLESSRUNNERCPP_API UClass* StaticClass<class AEndlessRunnerCppCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID EndlessRunnerCpp_Source_EndlessRunnerCpp_EndlessRunnerCppCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
