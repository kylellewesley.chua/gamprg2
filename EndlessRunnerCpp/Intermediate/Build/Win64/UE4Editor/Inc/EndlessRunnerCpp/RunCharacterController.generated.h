// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ENDLESSRUNNERCPP_RunCharacterController_generated_h
#error "RunCharacterController.generated.h already included, missing '#pragma once' in RunCharacterController.h"
#endif
#define ENDLESSRUNNERCPP_RunCharacterController_generated_h

#define EndlessRunnerCpp_Source_EndlessRunnerCpp_RunCharacterController_h_16_SPARSE_DATA
#define EndlessRunnerCpp_Source_EndlessRunnerCpp_RunCharacterController_h_16_RPC_WRAPPERS
#define EndlessRunnerCpp_Source_EndlessRunnerCpp_RunCharacterController_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define EndlessRunnerCpp_Source_EndlessRunnerCpp_RunCharacterController_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARunCharacterController(); \
	friend struct Z_Construct_UClass_ARunCharacterController_Statics; \
public: \
	DECLARE_CLASS(ARunCharacterController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunnerCpp"), NO_API) \
	DECLARE_SERIALIZER(ARunCharacterController)


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_RunCharacterController_h_16_INCLASS \
private: \
	static void StaticRegisterNativesARunCharacterController(); \
	friend struct Z_Construct_UClass_ARunCharacterController_Statics; \
public: \
	DECLARE_CLASS(ARunCharacterController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunnerCpp"), NO_API) \
	DECLARE_SERIALIZER(ARunCharacterController)


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_RunCharacterController_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARunCharacterController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARunCharacterController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunCharacterController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunCharacterController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunCharacterController(ARunCharacterController&&); \
	NO_API ARunCharacterController(const ARunCharacterController&); \
public:


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_RunCharacterController_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunCharacterController(ARunCharacterController&&); \
	NO_API ARunCharacterController(const ARunCharacterController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunCharacterController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunCharacterController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARunCharacterController)


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_RunCharacterController_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__RunCharacter() { return STRUCT_OFFSET(ARunCharacterController, RunCharacter); }


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_RunCharacterController_h_13_PROLOG
#define EndlessRunnerCpp_Source_EndlessRunnerCpp_RunCharacterController_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_RunCharacterController_h_16_PRIVATE_PROPERTY_OFFSET \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_RunCharacterController_h_16_SPARSE_DATA \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_RunCharacterController_h_16_RPC_WRAPPERS \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_RunCharacterController_h_16_INCLASS \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_RunCharacterController_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_RunCharacterController_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_RunCharacterController_h_16_PRIVATE_PROPERTY_OFFSET \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_RunCharacterController_h_16_SPARSE_DATA \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_RunCharacterController_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_RunCharacterController_h_16_INCLASS_NO_PURE_DECLS \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_RunCharacterController_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENDLESSRUNNERCPP_API UClass* StaticClass<class ARunCharacterController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID EndlessRunnerCpp_Source_EndlessRunnerCpp_RunCharacterController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
