// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ATile;
#ifdef ENDLESSRUNNERCPP_RunGameMode_generated_h
#error "RunGameMode.generated.h already included, missing '#pragma once' in RunGameMode.h"
#endif
#define ENDLESSRUNNERCPP_RunGameMode_generated_h

#define EndlessRunnerCpp_Source_EndlessRunnerCpp_RunGameMode_h_17_SPARSE_DATA
#define EndlessRunnerCpp_Source_EndlessRunnerCpp_RunGameMode_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnTileExited); \
	DECLARE_FUNCTION(execGetLatestTile);


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_RunGameMode_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnTileExited); \
	DECLARE_FUNCTION(execGetLatestTile);


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_RunGameMode_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARunGameMode(); \
	friend struct Z_Construct_UClass_ARunGameMode_Statics; \
public: \
	DECLARE_CLASS(ARunGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunnerCpp"), NO_API) \
	DECLARE_SERIALIZER(ARunGameMode)


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_RunGameMode_h_17_INCLASS \
private: \
	static void StaticRegisterNativesARunGameMode(); \
	friend struct Z_Construct_UClass_ARunGameMode_Statics; \
public: \
	DECLARE_CLASS(ARunGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunnerCpp"), NO_API) \
	DECLARE_SERIALIZER(ARunGameMode)


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_RunGameMode_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARunGameMode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARunGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunGameMode(ARunGameMode&&); \
	NO_API ARunGameMode(const ARunGameMode&); \
public:


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_RunGameMode_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARunGameMode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunGameMode(ARunGameMode&&); \
	NO_API ARunGameMode(const ARunGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunGameMode); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARunGameMode)


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_RunGameMode_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__beginTileCount() { return STRUCT_OFFSET(ARunGameMode, beginTileCount); } \
	FORCEINLINE static uint32 __PPO__TileSpawnClass() { return STRUCT_OFFSET(ARunGameMode, TileSpawnClass); }


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_RunGameMode_h_14_PROLOG
#define EndlessRunnerCpp_Source_EndlessRunnerCpp_RunGameMode_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_RunGameMode_h_17_PRIVATE_PROPERTY_OFFSET \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_RunGameMode_h_17_SPARSE_DATA \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_RunGameMode_h_17_RPC_WRAPPERS \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_RunGameMode_h_17_INCLASS \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_RunGameMode_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define EndlessRunnerCpp_Source_EndlessRunnerCpp_RunGameMode_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_RunGameMode_h_17_PRIVATE_PROPERTY_OFFSET \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_RunGameMode_h_17_SPARSE_DATA \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_RunGameMode_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_RunGameMode_h_17_INCLASS_NO_PURE_DECLS \
	EndlessRunnerCpp_Source_EndlessRunnerCpp_RunGameMode_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENDLESSRUNNERCPP_API UClass* StaticClass<class ARunGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID EndlessRunnerCpp_Source_EndlessRunnerCpp_RunGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
