// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tile.generated.h"

class AObstacle;
class APickup;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FExitTriggerSignature, ATile*, Tile);

UCLASS()
class ENDLESSRUNNERCPP_API ATile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATile();

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Tile Settings")
	float DestroyDelay = 5.0f;
	FTimerHandle DestroyDelayHandle;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Tile Settings")
	bool HasSpawn = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Tile Settings")
	TArray<TSubclassOf<AObstacle>> ObstacleArray;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Tile Settings")
	int32 minObstacles;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Tile Settings")
	int32 maxObstacles;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Tile Settings")
	TArray<TSubclassOf<APickup>> PickupArray;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Tile Settings")
	int32 minPickup;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Tile Settings")
	int32 maxPickup;

	UPROPERTY(BlueprintAssignable)
	FExitTriggerSignature OnExited;



protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	class USceneComponent* SceneComponent;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	class UArrowComponent* AttachPoint;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	class UBoxComponent* ExitTrigger;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	class UBoxComponent* BoundingBox;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	class UBoxComponent* CoinBoundingBox;


	


	



	UFUNCTION()
	void OnExit(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
	void DestroyTile();

	UFUNCTION()
	void SpawnObstacles();

	UFUNCTION()
	void SpawnPickupCoin();



	
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	FORCEINLINE UArrowComponent* GetAttachPoint() {
		return AttachPoint;
	}

};
