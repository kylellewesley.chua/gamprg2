// Fill out your copyright notice in the Description page of Project Settings.


#include "RunGameMode.h"
#include "Tile.h"
#include "Components/ArrowComponent.h"
void ARunGameMode::SpawnTile()
{
	if (latestTile == nullptr) {
		latestTile = GetWorld()->SpawnActor<ATile>(TileSpawnClass);
		latestTile->OnExited.AddDynamic(this, &ARunGameMode::OnTileExited);
	}
	else {
		FVector spawnLocation = latestTile->GetAttachPoint()->GetComponentLocation();
		latestTile = GetWorld()->SpawnActor<ATile>(TileSpawnClass, spawnLocation, latestTile->GetActorRotation());
		latestTile->OnExited.AddDynamic(this, &ARunGameMode::OnTileExited);
	}
	
	
}

void ARunGameMode::BeginPlay()
{
	Super::BeginPlay();

	for (int32 i = 0; i < beginTileCount; i++) {
		SpawnTile();
	}

}

void ARunGameMode::OnTileExited(ATile* tile)
{
	SpawnTile();
}
