// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RunGameMode.generated.h"

class ATile;

/**
 * 
 */
UCLASS()
class ENDLESSRUNNERCPP_API ARunGameMode : public AGameModeBase
{
	GENERATED_BODY()


public:
	ATile* latestTile;

	void SpawnTile();
	
	UFUNCTION(BlueprintPure)
	FORCEINLINE ATile* GetLatestTile() {
		return latestTile;
	}

	
protected:
	virtual void BeginPlay() override;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Variables")
		int32 beginTileCount;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Variables")
		TSubclassOf<ATile> TileSpawnClass;

	UFUNCTION()
	void OnTileExited(class ATile* tile);


};
