// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacterController.h"
#include "RunCharacter.h"





ARunCharacterController::ARunCharacterController() 
{
	

}


void ARunCharacterController::BeginPlay() 
{
	Super::BeginPlay();

	RunCharacter = Cast<ARunCharacter>(GetPawn());


}


void ARunCharacterController::MoveForward(float scale)
{
	RunCharacter->AddMovementInput(RunCharacter->GetActorForwardVector() * scale);

}

void ARunCharacterController::MoveRight(float scale)
{
	RunCharacter->AddMovementInput(RunCharacter->GetActorRightVector() * scale * RightSpeed);
}

void ARunCharacterController::Tick(float Deltatime)
{
	Super::Tick(Deltatime);
	if (!RunCharacter->isDead) {
		MoveForward(5.0f);
	}
	
}

void ARunCharacterController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis("MoveRight", this, &ARunCharacterController::MoveRight);
	//InputComponent->BindAxis("MoveForward", this, &ARunCharacterController::MoveForward);

}

