// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"


// Sets default values
ARunCharacter::ARunCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArm = CreateDefaultSubobject<USpringArmComponent>("Spring Arm");
	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");

	SpringArm->SetupAttachment(RootComponent);
	//SpringArm->bDoCollisionTest*(false);
	//SpringArm->bUsePawnControlRotation*(true);
	
	SpringArm->SocketOffset = FVector(0.0f, 0.0f, 100.0f);

	Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);
	SpringArm->SetRelativeLocation(localLocation);


	

}

// Called when the game starts or when spawned
void ARunCharacter::BeginPlay()
{
	Super::BeginPlay();
	


	
	
}

void ARunCharacter::Die()
{
	if (isDead) {
		return;
	}
	APlayerController* controller = Cast<APlayerController>(GetController());
	isDead = true;
	GetMesh()->SetVisibility(false);
	if (controller == nullptr) {
		return;
	}
	controller->DisableInput(controller);
	OnDie.Broadcast();
}

void ARunCharacter::AddCoin()
{
	coinCount++;
	GEngine->AddOnScreenDebugMessage(-1, 2, FColor::Red, *FString::FromInt(coinCount));
}

// Called every frame
void ARunCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ARunCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

