// Fill out your copyright notice in the Description page of Project Settings.


#include "Tile.h"
#include "Components/BoxComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/SceneComponent.h"
#include "TimerManager.h"
#include "Kismet/GameplayStatics.h"
#include "RunGameMode.h"
#include "RunCharacter.h"
#include "Obstacle.h"
#include "Pickup.h"
#include "Kismet/KismetMathLibrary.h"



// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	AttachPoint = CreateDefaultSubobject<UArrowComponent>("AttachPoint");
	ExitTrigger = CreateDefaultSubobject<UBoxComponent>("ExitTrigger");
	BoundingBox = CreateDefaultSubobject<UBoxComponent>("BoundingBox");
	CoinBoundingBox = CreateDefaultSubobject<UBoxComponent>("CoinBoundingBox");
	

	AttachPoint->SetupAttachment(SceneComponent);
	ExitTrigger->SetupAttachment(SceneComponent);
	BoundingBox->SetupAttachment(SceneComponent);
	CoinBoundingBox->SetupAttachment(SceneComponent);





}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();
	ExitTrigger->OnComponentEndOverlap.AddDynamic(this, &ATile::OnExit);

	SpawnObstacles();
	SpawnPickupCoin();

	
}

void ATile::OnExit(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	ACharacter* RunCharacter = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	if (HasSpawn == false && RunCharacter == Cast<ACharacter>(OtherActor)) {
		ARunGameMode* RunGameMode = Cast<ARunGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
		if (RunGameMode == nullptr) {
			return;
		}
		//RunGameMode->SpawnTile();
		GetWorld()->GetTimerManager().SetTimer(DestroyDelayHandle, this, &ATile::DestroyTile, DestroyDelay, false);
		HasSpawn = true;
		OnExited.Broadcast(this);
		
	}
	
	

}

void ATile::DestroyTile()
{
	GetWorld()->GetTimerManager().ClearTimer(DestroyDelayHandle);
	
	Destroy();
}

void ATile::SpawnObstacles()
{
	int32 obstacleCount = FMath::RandRange(minObstacles, maxObstacles);
	for (int32 i = 0; i < obstacleCount; i++) {
		TSubclassOf<AObstacle> obstacle = ObstacleArray[FMath::RandRange(0, ObstacleArray.Num()-1)];
		if (obstacle == nullptr) {
			continue;
		}
		FVector spawnPosition = UKismetMathLibrary::RandomPointInBoundingBox(BoundingBox->GetComponentLocation(), BoundingBox->GetScaledBoxExtent());
		AObstacle* spawnObstacle = GetWorld()->SpawnActor<AObstacle>(obstacle, spawnPosition, GetActorRotation());
		spawnObstacle->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
		OnExited.AddDynamic(spawnObstacle, &AObstacle::OnDestroyTile);
	}

}

void ATile::SpawnPickupCoin()
{
	int32 pickupCount = FMath::RandRange(minPickup, maxPickup);
	for (int32 i = 0; i < pickupCount; i++) {
		TSubclassOf<APickup> pickup = PickupArray[FMath::RandRange(0, PickupArray.Num() - 1)];
		if (pickup == nullptr) {
			continue;
		}
		FVector spawnPosition = UKismetMathLibrary::RandomPointInBoundingBox(CoinBoundingBox->GetComponentLocation(), CoinBoundingBox->GetScaledBoxExtent());
		APickup* spawnPickup = GetWorld()->SpawnActor<APickup>(pickup, spawnPosition, GetActorRotation());
		spawnPickup->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
		OnExited.AddDynamic(spawnPickup, &APickup::OnDestroyTile);
	}
	
}

// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);



}

