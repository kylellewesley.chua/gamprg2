// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "RunCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDieTriggerSignature);

UCLASS()
class ENDLESSRUNNERCPP_API ARunCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ARunCharacter();

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Character Settings")
	bool isDead = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Character Settings")
	int32 coinCount = 0;

	UFUNCTION(BlueprintCallable, Category = "Die Function")
	void Die();

	UFUNCTION(BlueprintCallable, Category = "Coin Function")
	void AddCoin();

	UPROPERTY(BlueprintAssignable)
	FDieTriggerSignature OnDie;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	
	UPROPERTY(VisibleAnywhere, Category = "Components")
	class USpringArmComponent* SpringArm;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	class UCameraComponent* Camera;

	UPROPERTY(VisibleAnywhere, Category = "Fields")
	FVector localLocation = FVector(0.0f, 0.0f, 0.0f);

	

	


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
